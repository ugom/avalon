#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "avalon.settings")
from django.conf import settings
from django.core.management import execute_from_command_line
import django
django.setup()

try:
    os.remove('db.sqlite3')
    shutil.rmtree('gm/migrations/')
except:
    pass
execute_from_command_line(['manage.py', 'makemigrations'])
execute_from_command_line(['manage.py', 'makemigrations', 'gm'])

execute_from_command_line(['manage.py', 'migrate'])

from gm.models import *

players = ['MPR', 'RYA', 'PED', 'BFA', 'NGE', 'TBE', 'PCR', 'SHA', 'UMA', 'ACO', 'AJA', 'AFA', 'AER', 'ABE', 'RYU', 'SSA', 'CIC', 'AOC', 'NJE', 'MLE', 'NGA', 'FLA', 'BRA', 'MPO', 'DGR', 'DLA', 'GBU', 'CSI', 'PLE']
#MPO = Marco Polo = MPR + PED

for p in players:
     Player.objects.get_or_create(tri=p)

def create_mission(good_oberon, mission_cards, date, data, missions, merlin_killed=False):
    g = Game.objects.create(good_oberon=good_oberon, mission_cards=mission_cards, date=date, merlin_killed=merlin_killed)

    for tri in data.keys():
        Membership.objects.create(game=g, player=Player.objects.get(tri=tri), position=data[tri][0], role=data[tri][1])

    for i, m in enumerate(missions):
        Mission.objects.create(game=g, number=i, leader=Player.objects.get(tri=m[1]), result=m[0])

# -----------------------------
data = {
	'MPR': [0, 'PEO'],
	'RYA': [1, 'GAL'],
	'PED': [2, 'MER'],
	'AER': [3, 'MOR'],
	'SSA': [4, 'MOD'],
	'TBE': [5, 'OBE'],
	'ACO': [6, 'ASS'],
	'UMA': [7, 'PEO'],
	'RYU': [8, 'PEO'],
	'ABE': [9, 'PER'],
}
missions = [('S', 'MPR'), ('F', 'RYA'), ('F', 'ACO'), ('S', 'RYA'), ('F', 'SSA')]
create_mission(True, True, '2016-12-07', data, missions)

# -----------------------------
data = {
	'MPR': [0, 'PEO'],
	'RYA': [1, 'PEO'],
	'AJA': [2, 'PER'],
	'NGE': [3, 'MOR'],
	'SHA': [4, 'OBE'],
	'SSA': [5, 'PEO'],
	'AFA': [6, 'ASS'],
	'ACO': [7, 'MOD'],
	'UMA': [8, 'MER'],
	'ABE': [9, 'PEO'],
}
missions = [('F', 'AFA'), ('S', 'MPR'), ('F', 'SSA'), ('F', 'MPR')]
create_mission(True, True, '2016-12-08', data, missions)

# -----------------------------
data = {
	'PED': [0, 'ASS'],
	'PCR': [1, 'PEO'],
	'RYA': [2, 'OBE'],
	'SHA': [3, 'PEO'],
	'AFA': [4, 'PEO'],
	'RYU': [5, 'MER'],
	'ACO': [6, 'MOD'],
	'SSA': [7, 'MOR'],
	'MPR': [8, 'PER'],
}
missions = [('S', 'SSA'), ('F', 'PED'), ('F', 'RYA'), ('S', 'RYU'), ('F', 'MPR')]
create_mission(True, True, '2016-12-09', data, missions)

# -----------------------------
data = {
	'MPR': [0, 'MOR'],
	'SHA': [1, 'PEO'],
	'SSA': [2, 'MER'],
	'NGE': [3, 'OBE'],
	'CIC': [4, 'PER'],
	'AFA': [5, 'PEO'],
	'UMA': [6, 'MOD'],
	'PCR': [7, 'PEO'],
}
missions = [('S', 'SSA'), ('F', 'PCR'), ('F', 'CIC'), ('S', 'SHA'), ('F', 'UMA')]
create_mission(good_oberon=False, mission_cards=False, date='2016-12-13', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'ASS'],
	'SHA': [1, 'PEO'],
	'UMA': [2, 'PEO'],
	'NGE': [3, 'PER'],
	'AFA': [4, 'MOR'],
	'RYU': [5, 'MER'],
	'ACO': [6, 'PEO'],
	'RYA': [7, 'OBE'],
	'PCR': [8, 'MOD'],
	'MPR': [9, 'PEO'],
}
missions = [('S', 'RYA'), ('F', 'UMA'), ('S', 'RYA'), ('F', 'SHA'), ('F', 'ACO')]
create_mission(good_oberon=False, mission_cards=True, date='2016-12-15', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'PEO'],
	'NGE': [1, 'MOR'],
	'UMA': [2, 'PEO'],
	'ACO': [3, 'PEO'],
	'SHA': [4, 'PEO'],
	'AOC': [5, 'PER'],
	'AFA': [6, 'MOD'],
	'PCR': [7, 'OBE'],
	'AJA': [8, 'PEO'],
	'PED': [9, 'ASS'],
}
missions = [('S', 'AOC'), ('F', 'ABE'), ('F', 'AOC'), ('F', 'ABE')]
create_mission(good_oberon=False, mission_cards=False, date='2016-12-19', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'MER'],
	'AFA': [1, 'PEO'],
	'UMA': [2, 'MOD'],
	'ACO': [3, 'OBE'],
	'SHA': [4, 'GAL'],
	'CIC': [5, 'MOR'],
	'RYA': [6, 'PER'],
	'PED': [7, 'PEO'],
}
missions = [('S', 'ACO'), ('F', 'ABE'), ('F', 'SHA'), ('S', 'PED'), ('F', 'UMA')]
create_mission(good_oberon=True, mission_cards=True, date='2016-12-20', data=data, missions=missions)

# -----------------------------
data = {
	'NJE': [0, 'MER'],
	'SSA': [1, 'PEO'],
	'AOC': [2, 'OBE'],
	'NGE': [3, 'ASS'],
	'ACO': [4, 'PEO'],
	'ABE': [5, 'PEO'],
	'PCR': [6, 'MOR'],
	'AFA': [7, 'PEO'],
	'UMA': [8, 'MOD'],
	'RYA': [9, 'PER'],
}
missions = [('S', 'ABE'), ('S', 'NJE'), ('S', 'SSA')]
create_mission(good_oberon=True, mission_cards=False, date='2016-12-21', data=data, missions=missions)

# -----------------------------
data = {
	'NJE': [0, 'PEO'],
	'SSA': [1, 'OBE'],
	'AOC': [2, 'MER'],
	'NGE': [3, 'PEO'],
	'ACO': [4, 'ASS'],
	'ABE': [5, 'MOD'],
	'PCR': [6, 'MOR'],
	'AFA': [7, 'PER'],
	'UMA': [8, 'PEO'],
	'RYA': [9, 'PEO'],
}
missions = [('S', 'PCR'), ('F', 'SSA'), ('F', 'PCR'), ('S', 'RYA'), ('F', 'AOC')]
create_mission(good_oberon=True, mission_cards=False, date='2016-12-21', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'PEO'],
	'NJE': [1, 'MOR'],
	'UMA': [2, 'MER'],
	'NGE': [3, 'PER'],
	'SSA': [4, 'PEO'],
	'AJA': [5, 'OBE'],
	'ACO': [6, 'MOD'],
	'TBE': [7, 'PEO'],
}
missions = [('S', 'ACO'), ('F', 'NGE'), ('S', 'ABE'), ('S', 'NJE')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2016-12-22', data=data, missions=missions)

# -----------------------------
data = {
	'NGE': [0, 'PER'],
	'MLE': [1, 'MER'],
	'SSA': [2, 'PEO'],
	'NGA': [3, 'OBE'],
	'ACO': [4, 'PEO'],
	'UMA': [5, 'MOD'],
	'AOC': [6, 'MOR'],
}
missions = [('S', 'SSA'), ('S', 'NGE'), ('S', 'MLE')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2016-12-23', data=data, missions=missions)

# -----------------------------
data = {
	'TBE': [0, 'OBE'],
	'RYA': [1, 'PER'],
	'PCR': [2, 'MOR'],
	'FLA': [3, 'MOD'],
	'SHA': [4, 'MER'],
	'ACO': [5, 'PEO'],
	'UMA': [6, 'PEO'],
	'PED': [7, 'PEO'],
}
missions = [('F', 'RYA'), ('S', 'UMA'), ('F', 'PED'), ('S', 'SHA'), ('S', 'RYA')]
create_mission(good_oberon=True, mission_cards=False, merlin_killed=True, date='2017-01-02', data=data, missions=missions)

# -----------------------------
data = {
	'TBE': [0, 'PEO'],
	'RYA': [1, 'MOD'],
	'PCR': [2, 'OBE'],
	'AOC': [3, 'MOR'],
	'SHA': [4, 'MER'],
	'ABE': [5, 'PER'],
	'UMA': [6, 'PEO'],
	'PED': [7, 'PEO'],
}
missions = [('S', 'ABE'), ('F', 'PED'), ('F', 'SHA'), ('F', 'RYA')]
create_mission(good_oberon=True, mission_cards=True, date='2017-01-03', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'PEO'],
	'PCR': [1, 'MER'],
	'UMA': [2, 'PER'],
	'SHA': [3, 'OBE'],
	'TBE': [4, 'MOR'],
	'AFA': [5, 'MOD'],
	'RYA': [6, 'PEO'],
	'CIC': [7, 'PEO'],
}
missions = [('S', 'RYA'), ('S', 'PCR'), ('S', 'UMA')]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-04', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'MER'],
	'AJA': [1, 'MOD'],
	'BRA': [2, 'PER'],
	'MPR': [3, 'OBE'],
	'SSA': [4, 'PEO'],
	'BFA': [5, 'PEO'],
	'AFA': [6, 'MOR'],
	'UMA': [7, 'PEO'],
}
missions = [('S', 'MPR'), ('F', 'ABE'), ('F', 'BFA'), ('S', 'BRA'), ('S', 'BFA')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-05', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'PER'],
	'RYU': [1, 'MOD'],
	'AFA': [2, 'PEO'],
	'SSA': [3, 'MER'],
	'RYA': [4, 'PEO'],
	'PED': [5, 'MOR'],
}
missions = [('S', 'RYU'), ('F', 'AFA'), ('S', 'ABE'), ('S', 'RYU')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=True, date='2017-01-06', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'MER'],
	'RYU': [1, 'MOR'],
	'AFA': [2, 'MOD'],
	'SSA': [3, 'PEO'],
	'RYA': [4, 'PER'],
	'PED': [5, 'PEO'],
}
missions = [('S', 'RYA'), ('S', 'SSA'), ('S', 'PED')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-06', data=data, missions=missions)

# -----------------------------
data = {
	'ABE': [0, 'PER'],
	'PED': [1, 'MOR'],
	'MPR': [2, 'PEO'],
	'ACO': [3, 'PEO'],
	'UMA': [4, 'MER'],
	'RYA': [5, 'MOD'],
	'SSA': [6, 'OBE'],
}
missions = [('S', 'MPR'), ('F', 'ABE'), ('S', 'UMA'), ('S', 'RYA')]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=True, date='2017-01-09', data=data, missions=missions)

# -----------------------------
data = {
	'TBE': [0, 'OBE'],
	'RYA': [1, 'PEO'],
	'NJE': [2, 'PEO'],
	'ABE': [3, 'MER'],
	'PCR': [4, 'PEO'],
	'UMA': [5, 'MOR'],
	'AJA': [6, 'PER'],
	'PED': [7, 'MOD'],
}
missions = [('S', 'RYA'), ('S', 'PCR'), ('S', 'UMA'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-10', data=data, missions=missions)

# -----------------------------
# changement règle good obéron: gagne avec les méchants
data = {
	'TBE': [0, 'OBE'],
	'AFA': [1, 'GAL'],
	'PCR': [2, 'PEO'],
	'MPO': [3, 'PEO'],
	'ACO': [4, 'MOD'],
	'SSA': [5, 'ASS'],
	'SHA': [6, 'MER'],
	'ABE': [7, 'PER'],
	'UMA': [8, 'PEO'],
	'RYA': [9, 'MOR'],
}
missions = [('F', 'TBE'), ('F', 'ACO'), ('F', 'UMA'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-11', data=data, missions=missions)

# -----------------------------
# changement règle good obéron: gagne avec les méchants
data = {
	'SHA': [0, 'MOD'],
	'RYA': [1, 'MER'],
	'AFA': [2, 'PEO'],
	'SSA': [3, 'PEO'],
	'MPR': [4, 'PER'],
	'ACO': [5, 'PEO'],
	'UMA': [6, 'MOR'],
	'ABE': [7, 'OBE'],
}
missions = [('F', 'SHA'), ('F', 'ACO'), ('F', 'RYA'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=True, date='2017-01-12', data=data, missions=missions)

data = {
	'PCR': [0, 'OBE'],
	'TBE': [1, 'ASS'],
	'ABE': [2, 'MOR'],
	'RYU': [3, 'MOD'],
	'PED': [4, 'PEO'],
	'SSA': [5, 'PEO'],
	'AFA': [6, 'MER'],
	'MPR': [7, 'PER'],
	'NGE': [8, 'PEO'],
	'RYA': [9, 'PEO'],
}
missions = [('F', 'PCR'), ('S', 'SSA'), ('F', 'AFA'), ('S', 'RYA'), ('S', 'PED'),]
create_mission(good_oberon=True, mission_cards=False, merlin_killed=True, date='2017-01-13', data=data, missions=missions)

data = {
	'SHA': [0, 'PEO'],
	'RYA': [1, 'MOD'],
	'PCR': [2, 'MOR'],
	'ACO': [3, 'PER'],
	'ABE': [4, 'PEO'],
	'SSA': [5, 'OBE'],
	'AFA': [6, 'MER'],
	'UMA': [7, 'PEO'],
	'PED': [8, 'PEO'],
}
missions = [('S', 'SHA'), ('F', 'SSA'), ('F', 'PED'), ('S', 'SHA'), ('S', 'ACO'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-16', data=data, missions=missions)

data = {
	'ABE': [0, 'PEO'],
	'PCR': [1, 'MER'],
	'CIC': [2, 'PER'],
	'ACO': [3, 'PEO'],
	'AFA': [4, 'OBE'],
	'RYU': [5, 'MOD'],
	'SSA': [6, 'PEO'],
	'UMA': [7, 'PEO'],
	'PED': [8, 'MOR'],
}
missions = [('F', 'PED'), ('S', 'ABE'), ('S', 'PCR'), ('S', 'CIC'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-17', data=data, missions=missions)

data = {
	'ABE': [0, 'PEO'],
	'PCR': [1, 'MOR'],
	'CIC': [2, 'MER'],
	'ACO': [3, 'PEO'],
	'AFA': [4, 'PEO'],
	'RYU': [5, 'OBE'],
	'SSA': [6, 'PEO'],
	'UMA': [7, 'PER'],
	'PED': [8, 'MOD'],
}
missions = [('S', 'SSA'), ('F', 'PCR'), ('F', 'SSA'), ('F', 'UMA'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-17', data=data, missions=missions)

data = {
	'SHA': [0, 'PEO'],
	'NJE': [1, 'MOD'],
	'AFA': [2, 'PEO'],
	'UMA': [3, 'MOR'],
	'RYU': [4, 'MER'],
	'ACO': [5, 'PEO'],
	'PED': [6, 'OBE'],
	'RYA': [7, 'PER'],
	'TBE': [8, 'PEO'],
}
missions = [('S', 'AFA'), ('F', 'UMA'), ('S', 'RYA'), ('S', 'ABE'),]
create_mission(good_oberon=True, mission_cards=False, merlin_killed=False, date='2017-01-18', data=data, missions=missions)

data = {
	'SHA': [0, 'MOD'],
	'NJE': [1, 'PER'],
	'AFA': [2, 'PEO'],
	'UMA': [3, 'PEO'],
	'RYU': [4, 'PEO'],
	'ACO': [5, 'MER'],
	'PED': [6, 'PEO'],
	'RYA': [7, 'MOR'],
	'ABE': [8, 'OBE'],
}
missions = [('F', 'RYU'), ('F', 'SHA'), ('F', 'RYA'),]
#create_mission(good_oberon=True, mission_cards=False, merlin_killed=False, blitz=True, date='2017-01-18', data=data, missions=missions)

data = {
	'AJA': [0, 'PEO'],
	'NGE': [1, 'OBE'],
	'ACO': [2, 'MER'],
	'AFA': [3, 'ASS'],
	'ABE': [4, 'PEO'],
	'RYU': [5, 'PER'],
	'PCR': [6, 'MOR'],
	'UMA': [7, 'PEO'],
	'MPR': [8, 'PEO'],
	'NGE': [9, 'MOD'],
}
missions = [('S', 'MPR'), ('F', 'AFA'), ('F', 'ABE'), ('S', 'MPR'), ('F', 'ACO'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=False, date='2017-01-19', data=data, missions=missions)

data = {
	'ABE': [0, 'PEO'],
	'SSA': [1, 'MOR'],
	'AFA': [2, 'OBE'],
	'MPR': [3, 'PEO'],
	'RYA': [4, 'PER'],
	'SHA': [5, 'PEO'],
	'ACO': [6, 'PEO'],
	'UMA': [7, 'MER'],
	'PED': [8, 'MOD'],
}
missions = [('F', 'SSA'), ('F', 'ACO'), ('S', 'ABE'), ('S', 'MPR'), ('S', 'UMA'),]
create_mission(good_oberon=True, mission_cards=True, merlin_killed=True, date='2017-01-20', data=data, missions=missions)

data = {
	'AJA': [0, 'PEO'],
	'NGE': [1, 'ASS'],
	'PED': [2, 'PEO'],
	'DGR': [3, 'MER'],
	'MPR': [4, 'PEO'],
	'ACO': [5, 'MOD'],
	'CIC': [6, 'PEO'],
	'SHA': [7, 'PER'],
	'PCR': [8, 'OBE'],
	'UMA': [9, 'MOR'],
}
missions = [('S', 'ACO'), ('F', 'AJA'), ('F', 'ACO'), ('F', 'PCR'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-21', data=data, missions=missions)

data = {
	'PCR': [0, 'MER'],
	'AJA': [1, 'PEO'],
	'MPO': [2, 'PEO'],
	'DGR': [3, 'MOD'],
	'SHA': [4, 'MOR'],
	'CIC': [5, 'PEO'],
	'ACO': [6, 'ASS'],
	'NGE': [7, 'PEO'],
	'SSA': [8, 'PER'],
	'UMA': [9, 'OBE'],
}
missions = [('F', 'SSA'), ('S', 'DGR'), ('S', 'MPO'), ('S', 'CIC'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=True, date='2017-01-21', data=data, missions=missions)

data = {
	'PCR': [0, 'PEO'],
	'AJA': [1, 'ASS'],
	'MPO': [2, 'PEO'],
	'DGR': [3, 'MOR'],
	'SHA': [4, 'MER'],
	'CIC': [5, 'MOD'],
	'ACO': [6, 'OBE'],
	'NGE': [7, 'PER'],
	'SSA': [8, 'PEO'],
	'UMA': [9, 'PEO'],
}
missions = [('F', 'PCR'), ('F', 'SHA'), ('S', 'UMA'), ('S', 'AJA'), ('S', 'SHA'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-21', data=data, missions=missions)

data = {
	'SSA': [0, 'OBE'],
	'SHA': [1, 'ASS'],
	'PCR': [2, 'PEO'],
	'RYA': [3, 'MOD'],
	'TBE': [4, 'MER'],
	'RYU': [5, 'MOR'],
	'ABE': [6, 'PEO'],
	'AFA': [7, 'PEO'],
	'UMA': [8, 'PER'],
	'PED': [9, 'PEO'],
}
missions = [('S', 'PCR'), ('S', 'AFA'), ('S', 'UMA'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-23', data=data, missions=missions)

data = {
	'SSA': [0, 'MER'],
	'SHA': [1, 'PEO'],
	'PCR': [2, 'MOD'],
	'RYA': [3, 'MOR'],
	'TBE': [4, 'PER'],
	'RYU': [5, 'PEO'],
	'ABE': [6, 'OBE'],
	'AFA': [7, 'PEO'],
	'UMA': [8, 'ASS'],
	'PED': [9, 'PEO'],
}
missions = [('S', 'RYA'), ('F', 'UMA'), ('F', 'RYA'), ('F', 'UMA'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-23', data=data, missions=missions)

data = {
	'ABE': [0, 'MOR'],
	'PED': [1, 'PER'],
	'GBU': [2, 'MOD'],
	'DLA': [3, 'PEO'],
	'UMA': [4, 'PEO'],
	'AFA': [5, 'MER'],
	'SHA': [6, 'PEO'],
	'RYA': [7, 'ASS'],
}
missions = [('S', 'DLA'), ('F', 'RYA'), ('F', 'PED'), ('S', 'DLA'), ('F', 'SHA'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-24', data=data, missions=missions)

data = {
	'AJA': [0, 'PEO'],
	'DLA': [1, 'PEO'],
	'AFA': [2, 'ASS'],
	'SHA': [3, 'PEO'],
	'ACO': [4, 'MOD'],
	'UMA': [5, 'MER'],
	'PCR': [6, 'PER'],
	'PED': [7, 'MOR'],
}
missions = [('S', 'AJA'), ('F', 'UMA'), ('F', 'AFA'), ('S', 'UMA'), ('F', 'PCR'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-25', data=data, missions=missions)

data = {
	'ABE': [0, 'MER'],
	'RYA': [1, 'PEO'],
	'AJA': [2, 'PER'],
	'RYU': [3, 'ASS'],
	'AFA': [4, 'PEO'],
	'UMA': [5, 'PEO'],
	'ACO': [6, 'MOR'],
	'SHA': [7, 'MOD'],
	'CSI': [8, 'PEO'],
	'PCR': [9, 'OBE'],
}
missions = [('S', 'ABE'), ('F', 'UMA'), ('F', 'ABE'), ('F', 'RYA'),]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-26', data=data, missions=missions)

data = {
        'ABE': [0, 'PER'],
        'PED': [1, 'MER'],
        'AER': [2, 'PEO'],
        'TBE': [3, 'MOD'],
        'SHA': [4, 'PEO'],
        'AJA': [5, 'MOR'],
}
missions = [('S', 'AER'), ('F', 'ABE'), ('S', 'AER'), ('S', 'TBE')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-27', data=data, missions=missions)

data = {
        'ABE': [0, 'MER'],
        'CIC': [1, 'MOR'],
        'AFA': [2, 'PER'],
        'UMA': [3, 'PEO'],
        'PCR': [4, 'MOD'],
        'PED': [5, 'PEO'],
}
missions = [('S', 'CIC'), ('F', 'UMA'), ('S', 'AFA'), ('S', 'UMA')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-30', data=data, missions=missions)

data = {
        'ABE': [0, 'MER'],
        'SHA': [1, 'MOR'],
        'CIC': [2, 'PEO'],
        'AFA': [3, 'PER'],
        'UMA': [4, 'MOD'],
        'PCR': [5, 'OBE'],
        'PED': [6, 'PEO'],
}
missions = [('S', 'CIC'), ('S', 'ABE'), ('F', 'SHA'), ('S', 'AFA')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-30', data=data, missions=missions)

data = {
        'SHA': [0, 'PEO'],
        'PCR': [1, 'MOD'],
        'AFA': [2, 'PEO'],
        'CIC': [3, 'PER'],
        'UMA': [4, 'MOR'],
        'ACO': [5, 'OBE'],
        'PED': [6, 'MER'],
}
missions = [('F', 'ACO'), ('F', 'SHA'), ('F', 'PCR')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-31', data=data, missions=missions)

data = {
        'RYU': [0, 'ASS'],
        'SHA': [1, 'MER'],
        'PCR': [2, 'PEO'],
        'RYA': [3, 'PEO'],
        'AFA': [4, 'PEO'],
        'CIC': [5, 'MOR'],
        'ABE': [6, 'OBE'],
        'UMA': [7, 'PEO'],
        'ACO': [8, 'PER'],
        'PED': [9, 'MOD'],
}
missions = [('F', 'ABE'), ('S', 'SHA'), ('S', 'PCR'), ('S', 'PCR'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-01-31', data=data, missions=missions)

#data = {
#        'ABE': [0, 'PEO'],
#        'RYA': [1, 'MOR'],
#        'AFA': [2, 'PEO'],
#        'SSA': [3, 'MOD'],
#        'UMA': [4, 'PER'],
#        'ACO': [5, 'MER'],
#}
##missions = []
#create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-01', data=data, missions=missions)

data = {
        'RYA': [0, 'OBE'],
        'MPR': [1, 'MOR'],
        'AFA': [2, 'MER'],
        'ABE': [3, 'PER'],
        'RYU': [4, 'PEO'],
        'ACO': [5, 'PEO'],
        'PCR': [6, 'PEO'],
        'SHA': [7, 'MOD'],
}
missions = [('S', 'ACO'), ('S', 'RYU'), ('S', 'ABE'), ]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-02', data=data, missions=missions)

data = {
        'PED': [0, 'MOR'],
        'AOC': [1, 'PER'],
        'RYA': [2, 'OBE'],
        'PCR': [3, 'MOD'],
        'AER': [4, 'PEO'],
        'DLA': [5, 'PEO'],
        'GBU': [6, 'PEO'],
        'ACO': [7, 'PEO'],
        'SSA': [8, 'MER'],
}

missions = [('S', 'SSA'), ('F', 'RYA'), ('F', 'AER'), ('F', 'PED')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-03', data=data, missions=missions)

data = {
        'ABE': [0, 'PEO'],
        'GBU': [1, 'MOD'],
        'SHA': [2, 'PER'],
        'AFA': [3, 'PEO'],
        'RYA': [4, 'OBE'],
        'PLE': [5, 'MOR'],
        'PCR': [6, 'PEO'],
        'UMA': [7, 'MER'],
}

missions = [('F', 'PLE'), ('F', 'GBU'), ('S', 'PCR'), ('S', 'SHA'), ('F', 'UMA')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-06', data=data, missions=missions)

data = {
        'ABE': [0, 'OBE'],
        'SHA': [1, 'MOR'],
        'RYA': [2, 'PEO'],
        'AFA': [3, 'PEO'],
        'PCR': [4, 'PEO'],
        'UMA': [5, 'PER'],
        'RYU': [6, 'MOD'],
        'TBE': [7, 'MER'],
}

missions = [('S', 'AFA'), ('S', 'PCR'), ('S', 'UMA')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-08', data=data, missions=missions)

data = {
        'ABE': [0, 'PER'],
        'SHA': [1, 'PEO'],
        'RYA': [2, 'PEO'],
        'AFA': [3, 'PEO'],
        'PCR': [4, 'MOR'],
        'UMA': [5, 'OBE'],
        'RYU': [6, 'MER'],
        'TBE': [7, 'MOD'],
}

missions = [('S', 'RYA'), ('F', 'TBE'), ('F', 'PCR'), ('S', 'RYU'), ('F', 'TBE')]
create_mission(good_oberon=False, mission_cards=False, merlin_killed=False, date='2017-02-08', data=data, missions=missions)


# --
for gamestats in GameStats.objects.all():
    gamestats.save()
for playerstats in PlayerStats.objects.all():
    playerstats.save()
update_rankings()
