# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
from gm.models import Game, GameStats, Membership, Mission, Player, PlayerStats, update_rankings
from gm import highcharts

def _get_all_lines(table):
    return pd.DataFrame(list(table.objects.all().values()))

def update_context(ctx):
    games = _get_all_lines(Game)
    game_stats = _get_all_lines(GameStats)
    memberships = _get_all_lines(Membership)
    missions = _get_all_lines(Mission)
    players = _get_all_lines(Player)
    player_stats = _get_all_lines(PlayerStats)

    players = players.set_index("id")
    missions["leader"] = [players.loc[i].tri for i in missions.leader_id]
    missions["success"] = missions["result"] == 'S'

    """
    print(games.head())
    print(game_stats.head())
    print(memberships.head()
    print(missions.head())
    print(players.head())
    print(player_stats.head())
    """
    chart_data = {
        "tri": [],
        "success_rate": [],
        "missions": []
    }

    for tri, df_ in missions.groupby("leader"):
        if len(df_) < 2:
            continue

        chart_data["tri"].append(tri)
        chart_data["success_rate"].append(100.*len(df_[df_.success]) / len(df_))
        chart_data["missions"].append(len(df_))


    print(chart_data)
    chart = highcharts.HighChart(
        render_to="mission_chart", title="Missions statistics", ylabel="",
        xlabel="Trigrams", subtitle="Success rages (%) and number of missions led by trigram. At least two missions required.",
        height=600)

    chart.plot_df(pd.DataFrame(chart_data).set_index("tri").sort("missions", ascending=False),
                  series=["success_rate", "missions"],
                  series_names=["Success Rate", "Nb Missions"],
                  type="column")

    chart.set_options("tooltip", valueDecimals=0)
    ctx["mission_stats"]["mission_chart"] = chart.get_chart().encode("utf-8")


