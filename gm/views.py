# -*- coding: utf-8 -*-
from __future__ import division
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count, F, Max, Min, Q
from django.shortcuts import redirect
from django.views import generic
from gm.models import Game, GameStats, Membership, Mission, Player, PlayerStats, update_rankings
from gm import ped

# todo: move that the GameStats manager
MIN_GAME_STATS = 4


def r(n):
    return round(n, 2)

def j(queryset):
    ''' join a queryset. '''
    return ', '.join([s[0] for s in queryset])

def displayedStats():

    gs = GameStats.objects

    stats = {}
    stats['n_game'] = gs.all().count()
    stats['n_evilwon'] = gs.filter(good_win=False).count()
    stats['n_goodwon'] = gs.filter(good_win=True).count()
    stats['n_evilwon_ratio'] = r(stats['n_evilwon'] / stats['n_game'] * 100)
    stats['n_goodwon_ratio'] = r(stats['n_goodwon'] / stats['n_game'] * 100)

    # Sylvain et Marc dans la même équipe
    gs_filt = Game.objects.filter(players__tri__contains='SSA').filter(players__tri__contains='MPR')
    stats['n_ssa_and_mpr_game'] = gs_filt.count()
    stats['n_ssa_and_mpr'] = 0
    for q in gs_filt:
        a = q.membership_set.get(player__tri='SSA').good
        b = q.membership_set.get(player__tri='MPR').good
        if a == b:
            stats['n_ssa_and_mpr'] += 1
    stats['n_ssa_and_mpr_ratio'] =  r(stats['n_ssa_and_mpr'] / stats['n_ssa_and_mpr_game'] * 100)

    # Stats quand PED est là / pas là
    gs_filt = Game.objects.filter(players__tri__contains='PED')
    stats['withPED_n_evilwon'] = gs_filt.filter(stats__good_win=False).count()
    stats['withPED_n_game'] = gs_filt.count()
    stats['withPED_n_evilwon_ratio'] = r(stats['withPED_n_evilwon'] / stats['withPED_n_game'] * 100)

    gs_filt = Game.objects.exclude(players__tri__contains='PED')
    stats['withoutPED_n_evilwon'] = gs_filt.filter(stats__good_win=False).count()
    stats['withoutPED_n_game'] = gs_filt.count()
    stats['withoutPED_n_evilwon_ratio'] = r(stats['withoutPED_n_evilwon'] / stats['withoutPED_n_game'] * 100)

    # à côté d'un méchant, y'a un méchant
    stats['n_mechmech'] = gs.filter(mechmech=True).count()
    stats['n_mechmech_ratio'] = r(stats['n_mechmech'] / stats['n_game'] * 100)

    # tbe is oberon
    stats['tbe_oberon'] = gs.filter(oberon__tri='TBE').count()
    stats['tbe_n_game'] = PlayerStats.objects.filter(player__tri='TBE')[0].n_game
    stats['tbe_oberon_ratio'] = r(stats['tbe_oberon'] / stats['tbe_n_game'] * 100)

    # best player
    stats['best_player_ratio'] = PlayerStats.objects.filter(n_game__gt=MIN_GAME_STATS).exclude(player__tri='AJA').aggregate(Max('pc_win'))['pc_win__max']
    stats['best_player'] = j(PlayerStats.objects.filter(n_game__gt=MIN_GAME_STATS).exclude(player__tri='AJA').filter(pc_win=stats['best_player_ratio']).values_list('player__tri'))
    stats['best_player_ratio'] = r(stats['best_player_ratio'])

    # roles
    for role in ['merlin', 'perceval', 'mordred', 'morgana', 'oberon', 'assassin']:
        stats['most_frequent_{}_ratio'.format(role)] = PlayerStats.objects.filter(n_game__gt=9).aggregate(Max('n_played_{}_ratio'.format(role)))['n_played_{}_ratio__max'.format(role)]
        kwargs = {'n_played_{}_ratio'.format(role): stats['most_frequent_{}_ratio'.format(role)], 'n_game__gt':9}
        stats['most_frequent_{}'.format(role)] = j(PlayerStats.objects.filter(**kwargs).values_list('player__tri'))
        stats['most_frequent_{}_ratio'.format(role)] = r(stats['most_frequent_{}_ratio'.format(role)])

    # never/always arthur
    na = PlayerStats.objects.all().filter(n_game__gt=MIN_GAME_STATS).exclude(player__tri='AJA')
    na = na.filter(n_arthur_ratio=na.aggregate(Min('n_arthur_ratio'))['n_arthur_ratio__min']).values_list('player__tri', 'n_arthur_ratio', 'n_mission_played')
    stats['never_arthur'] = j(na)
    stats['never_arthur_n_missions'] = na[0][2]
    stats['never_arthur_ratio'] = r(na[0][1])

    aa = PlayerStats.objects.all().filter(n_game__gt=MIN_GAME_STATS).exclude(player__tri='AJA')
    aa = aa.filter(n_arthur_ratio=aa.aggregate(Max('n_arthur_ratio'))['n_arthur_ratio__max']).values_list('player__tri', 'n_arthur_ratio', 'n_mission_played')
    stats['always_arthur'] = j(aa)
    stats['always_arthur_n_missions'] = aa[0][2]
    stats['always_arthur_ratio'] = r(aa[0][1])

    return stats

def missionStats():
    stats = {}
    l = Mission.objects.all()
    stats['count'] = l.count()
    stats['count_fail'] = l.filter(result=Mission.FAIL).count()
    stats['count_success'] = l.filter(result=Mission.SUCCESS).count()
    ea = l.filter(result=Mission.FAIL).values_list('leader__tri').annotate(count=Count('leader__tri'))
    stats['evil_arthur'] = list(ea.filter(count=ea.aggregate(Max('count'))['count__max']))
    ga = l.filter(result=Mission.SUCCESS).values_list('leader__tri').annotate(count=Count('leader__tri'))
    stats['good_arthur'] = list(ga.filter(count=ga.aggregate(Max('count'))['count__max']))

    em = l.filter(result=Mission.FAIL).filter(leader__tri=F('game__stats__merlin__tri')).values_list('leader__tri').annotate(count=Count('leader__tri'))
    stats['evil_merlin'] = list(em.filter(count=em.aggregate(Max('count'))['count__max']))
    gm = l.filter(result=Mission.SUCCESS).filter(leader__tri=F('game__stats__merlin__tri')).values_list('leader__tri').annotate(count=Count('leader__tri'))
    stats['good_merlin'] = list(gm.filter(count=gm.aggregate(Max('count'))['count__max']))

    return stats


class IndexView(generic.TemplateView):
    template_name = 'gm/home.html'
    context_object_name = 'gm_list'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['game_list'] = Game.objects.order_by('-date')
        context['player_list'] = Player.objects.filter(stats__n_game__gt=MIN_GAME_STATS).exclude(tri='AJA').order_by('-stats__pc_win')
        context['stats'] = displayedStats()
        context['mission_stats'] = missionStats()
        context['player_rank'] = Player.objects.filter((Q(stats__n_game__gt=MIN_GAME_STATS) & (~Q(tri='AJA')))| Q(tri='EVI') | Q(tri='GOO')).order_by('-stats__ranking_trueskill')
        context['min_game'] = MIN_GAME_STATS

        ped.update_context(context)
        return context


class GameCreateView(generic.edit.CreateView):
    model = Game
    fields = ['players', 'good_oberon', 'mission_cards', 'date']


class GameDetailView(generic.DetailView):
    model = Game


class PlayerCreateView(generic.edit.CreateView):
    model = Player
    fields = ['tri', ]
    success_url = reverse_lazy('gm:index')


def updateGameStats(request):
    for gamestats in GameStats.objects.all():
        gamestats.save()
    for playerstats in PlayerStats.objects.all():
        playerstats.save()
    update_rankings()
    return redirect('gm:index')

