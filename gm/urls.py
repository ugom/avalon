from django.conf.urls import url
#from django.core.urlresolvers import reverse

from gm import views

app_name = 'gm'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^create$', views.GameCreateView.as_view(), name='create'),
    url(r'^player_create$', views.PlayerCreateView.as_view(), name='player_create'),
    url(r'^(?P<pk>[-\w]+)/details$', views.GameDetailView.as_view(), name='details'),
    url(r'^stats_update$', views.updateGameStats, name='stats_update'),
]
