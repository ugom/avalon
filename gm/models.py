# -*- coding: utf-8 -*-
from __future__ import division
from django.db import models
from django.db.models import Count, Sum
from django.core.validators import validate_comma_separated_integer_list
from numpy import diff
import trueskill

class Player(models.Model):
    tri = models.CharField(max_length=3)

    def save(self, *args, **kwargs):
        super(Player, self).save(*args, **kwargs)
        PlayerStats.objects.get_or_create(player=self)

class Game(models.Model):
    players = models.ManyToManyField(
        Player,
        through='Membership',
        through_fields=('game', 'player'    )
    )
    good_oberon = models.BooleanField()
    mission_cards = models.BooleanField()
    date = models.DateField()
    merlin_killed = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(Game, self).save(*args, **kwargs)
        self.update_or_create_stats()

    def update_or_create_stats(self):
        GameStats.objects.get_or_create(game=self)

    def get_role(self, role):
        try:
             return self.membership_set.filter(role=role).first().player
        except:
             return None

'''
    def get_roles(self, role):
        return ' '.join([v['player__tri'] for v in self.membership_set.filter(role=role).values('player__tri')])
'''

class Mission(models.Model):
    FAIL = 'F'
    SUCCESS = 'S'

    MISSION_RESULTS = (
        (FAIL, 'Fail'),
        (SUCCESS, 'Success'),
    )

    game = models.ForeignKey(Game, related_name='missions')
    result = models.CharField(max_length=2, choices=MISSION_RESULTS)
    leader = models.ForeignKey(Player)
    number = models.IntegerField()


class Membership(models.Model):
    PEON = 'PEO'
    GALAHAD = 'GAL'
    PERCEVAL = 'PER'
    MERLIN = 'MER'
    MORGANA = 'MOR'
    ASSASSIN = 'ASS'
    OBERON = 'OBE'
    MORDRED = 'MOD'

    ROLE_CHOICES = (
        (PEON, 'Peon'),
        (GALAHAD, 'Galahad'),
        (PERCEVAL, 'Perceval'),
        (MERLIN, 'Merlin'),
        (MORGANA, 'Morgana'),
        (ASSASSIN, 'Assassin'),
        (OBERON, 'Oberon'),
        (MORDRED, 'Mordred'),
    )

    player = models.ForeignKey(Player)
    game = models.ForeignKey(Game)
    position = models.IntegerField()
    role = models.CharField(
        max_length=3,
        choices=ROLE_CHOICES,
        default=PEON,
    )

    @property
    def bad(self):
        return self.role in [Membership.MORGANA, Membership.ASSASSIN, Membership.OBERON, Membership.MORDRED]

    @property
    def good(self):
        return not self.bad

    @property
    def faction(self):
        return 'good' if self.good() else 'evil'


class GameStats(models.Model):
    game = models.OneToOneField(Game, related_name='stats')

    good_win = models.BooleanField()
    n_players = models.IntegerField()

    merlin = models.ForeignKey(Player, blank=True, null=True, related_name='merlin')
    perceval = models.ForeignKey(Player, blank=True, null=True, related_name='perceval')
    galahad = models.ForeignKey(Player, blank=True, null=True, related_name='galahad')
    mordred = models.ForeignKey(Player, blank=True, null=True, related_name='mordred')
    morgana = models.ForeignKey(Player, blank=True, null=True, related_name='morgana')
    oberon = models.ForeignKey(Player, blank=True, null=True, related_name='oberon')
    assassin = models.ForeignKey(Player, blank=True, null=True, related_name='assassin')

    evil_team_tri = models.CharField(max_length=255)
    good_team_tri = models.CharField(max_length=255)

    mechants_pos = models.CharField(validators=[validate_comma_separated_integer_list], max_length=255)
    mechmech = models.BooleanField(help_text='deux méchant sont à côté')

    def save(self, *args, **kwargs):
        self.good_win = self.game.missions.filter(result='S').count() >= 3 and not self.game.merlin_killed
        self.n_players = self.game.players.all().count()

        self.merlin = self.game.get_role(role=Membership.MERLIN)
        self.perceval = self.game.get_role(role=Membership.PERCEVAL)
        self.galahad = self.game.get_role(role=Membership.GALAHAD)
        self.mordred = self.game.get_role(role=Membership.MORDRED)
        self.morgana = self.game.get_role(role=Membership.MORGANA)
        self.oberon = self.game.get_role(role=Membership.OBERON)
        self.assassin = self.game.get_role(role=Membership.ASSASSIN)

        pos = []
        tris = []
        for p in [self.mordred, self.morgana, self.oberon, self.assassin]:
            if p is not None:
                tris.append(p.tri)
                pos.append(p.membership_set.filter(game=self.game).first().position)
        self.mechants_pos = sorted(pos)
        self.evil_team_tri = ','.join(sorted(tris))
        self.mechmech = len(self.mechants_pos) and (any(diff(self.mechants_pos) == 1) or (self.mechants_pos[-1] - self.mechants_pos[0] == self.n_players - 1))

        tris = []
        for p in self.game.players.values_list('tri'):
            p = p[0]
            if p not in self.evil_team_tri:
                tris.append(p)
        self.good_team_tri = ','.join(sorted(tris))

        super(GameStats, self).save(*args, **kwargs)


class PlayerStats(models.Model):
    player = models.OneToOneField(Player, related_name='stats')

    n_game = models.IntegerField(default=0)
    n_game_evil = models.IntegerField(default=0)
    n_game_good = models.IntegerField(default=0)

    n_win = models.IntegerField(default=0)
    n_win_evil = models.IntegerField(default=0)
    n_win_good = models.IntegerField(default=0)

    pc_win = models.FloatField(default=0)
    pc_win_evil = models.FloatField(default=0)
    pc_win_good = models.FloatField(default=0)

    n_played_merlin = models.IntegerField(default=0)
    n_played_perceval = models.IntegerField(default=0)
    n_played_galahad = models.IntegerField(default=0)
    n_played_mordred = models.IntegerField(default=0)
    n_played_morgana = models.IntegerField(default=0)
    n_played_oberon = models.IntegerField(default=0)
    n_played_assassin = models.IntegerField(default=0)

    n_played_merlin_ratio = models.FloatField(default=0)
    n_played_perceval_ratio = models.FloatField(default=0)
    n_played_galahad_ratio = models.FloatField(default=0)
    n_played_mordred_ratio = models.FloatField(default=0)
    n_played_morgana_ratio = models.FloatField(default=0)
    n_played_oberon_ratio = models.FloatField(default=0)
    n_played_assassin_ratio = models.FloatField(default=0)

    ranking_trueskill = models.FloatField(default=0)

    n_arthur = models.IntegerField(default=0)
    n_mission_played = models.IntegerField(default=0)
    n_arthur_ratio = models.FloatField(default=0)

    def save(self, *args, **kwargs):
        if self.player.tri in ['EVI', 'GOO']:
            return super(PlayerStats, self).save(*args, **kwargs)

        gs_filt = Game.objects.filter(players=self.player)
        gs_filt_evil = gs_filt.filter(stats__evil_team_tri__contains=self.player.tri)
        gs_filt_good = gs_filt.exclude(stats__evil_team_tri__contains=self.player.tri)

        self.n_game = gs_filt.count()
        self.n_game_evil = gs_filt_evil.count()
        self.n_game_good = gs_filt_good.count()

        self.n_win_evil = gs_filt_evil.filter(stats__good_win=False).count()
        self.n_win_good = gs_filt_good.filter(stats__good_win=True).count()
        self.n_win = self.n_win_evil + self.n_win_good

        if self.n_game:
            self.pc_win = self.n_win * 100 / self.n_game
        if self.n_game_good:
            self.pc_win_good = self.n_win_good * 100 / self.n_game_good
        if self.n_game_evil:
            self.pc_win_evil = self.n_win_evil * 100 / self.n_game_evil

        self.n_played_merlin = Membership.objects.filter(player=self.player).filter(role=Membership.MERLIN).count()
        self.n_played_perceval = Membership.objects.filter(player=self.player).filter(role=Membership.PERCEVAL).count()
        self.n_played_galahad = Membership.objects.filter(player=self.player).filter(role=Membership.GALAHAD).count()
        self.n_played_mordred = Membership.objects.filter(player=self.player).filter(role=Membership.MORDRED).count()
        self.n_played_morgana = Membership.objects.filter(player=self.player).filter(role=Membership.MORGANA).count()
        self.n_played_oberon = Membership.objects.filter(player=self.player).filter(role=Membership.OBERON).count()
        self.n_played_assassin = Membership.objects.filter(player=self.player).filter(role=Membership.ASSASSIN).count()

        if self.n_game:
            self.n_played_merlin_ratio = self.n_played_merlin / self.n_game * 100
            self.n_played_perceval_ratio = self.n_played_perceval / self.n_game * 100
            self.n_played_galahad_ratio = self.n_played_galahad / self.n_game * 100
            self.n_played_mordred_ratio = self.n_played_mordred / self.n_game * 100
            self.n_played_morgana_ratio = self.n_played_morgana / self.n_game * 100
            self.n_played_oberon_ratio = self.n_played_oberon / self.n_game * 100
            self.n_played_assassin_ratio = self.n_played_assassin / self.n_game * 100

        self.n_arthur = Mission.objects.all().filter(leader=self.player).count()
        self.n_mission_played = Game.objects.all().filter(players=self.player).annotate(count=Count('missions')).aggregate(r=Sum('count'))['r'] or 0
        if self.n_mission_played > 0:
            self.n_arthur_ratio = self.n_arthur / self.n_mission_played * 100

        super(PlayerStats, self).save(*args, **kwargs)

def update_rankings():
    ps = PlayerStats.objects

    ratings = {tri[0]: trueskill.Rating() for tri in Player.objects.values_list('tri')}
    ratings['EVI'] = trueskill.Rating(55)
    ratings['GOO'] = trueskill.Rating(-5)
    for game in GameStats.objects.all().order_by('game__date'):
        evil_tri = game.evil_team_tri + ',EVI'
        good_tri = game.good_team_tri + ',GOO'
        evil_team = [ratings[tri] for tri in evil_tri.split(',')]
        good_team = [ratings[tri] for tri in good_tri.split(',')]
        rank = int(game.good_win)
        # lower rank is better
        new_evil_rating, new_good_rating = trueskill.rate([evil_team, good_team], ranks=[rank, 1-rank])
        for new_rating, trigram in zip(new_evil_rating, evil_tri.split(',')):
            ratings[trigram] = new_rating
        for new_rating, trigram in zip(new_good_rating, good_tri.split(',')):
            ratings[trigram] = new_rating

    for tri, rating in ratings.items():
        ps.filter(player__tri=tri).update(ranking_trueskill=float(rating))

    # dummy player objects
    Player.objects.get_or_create(tri='EVI')
    Player.objects.get_or_create(tri='GOO')
    player, created = Player.objects.update_or_create(tri='EVI', defaults={'stats__ranking_trueskill': float(ratings['EVI'])})
    player, created = Player.objects.update_or_create(tri='GOO', defaults={'stats__ranking_trueskill': float(ratings['GOO'])})

    print('Evil ranking:', ratings['EVI'])
    print('Good ranking:', ratings['GOO'])


