import pandas as pd
import numpy as np
import json
from copy import deepcopy

DEFAULT_CHART = {
    "chart": {
        "backgroundColor": "transparent",
        "zoomType": "x",
        "plotBorderWidth": 1,
        "plotBorderColor": "#DDD",
        "style": {
            "boxShadow": "2px 120px 1px -120px rgba(0, 0, 0, 0.3)",
            "marginBottom": "15px",
        }
    },
    "colors": [
            #'#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'
            #'#38D018', '#F7A35C', "#00B4FF", '#A91B72', '#436E90', '#FF808D',
            'rgba(69, 114, 167, 1.0)', 'rgba(170, 70, 67, 1.0)',
            'rgba(137, 165, 78, 1.0)', 'rgba(128, 105, 155, 1.0)',
            'rgba(61, 150, 174, 1.0)', 'rgba(219, 132, 61, 1.0)',
            'rgba(146, 168, 205, 1.0)', 'rgba(164, 125, 124, 1.0)',
            'rgba(181, 202, 146, 1.0)'
        ],
    "tooltip": {
        "shared": True,
        "crosshairs": True, # blue background on hover
        "animation": False,
        "borderColor": "#CCC",
        "borderRadius": 0,
        "borderWidth": 3,
        "shadow": False,
        "valueDecimals": 3,
        "style": {}
    },
    "xAxis": {
        "labels": {
            "style": {
                "color": "#555",
                "fontWeight": "bold",
                "fontSize": 12
            }
        },
        "title": {
            "margin": 12,
            "style": {
                "fontWeight": "bold",
                "fontSize": "14px",
                "color": "#65355E"
            }
        }
    },
    "yAxis": {
        "labels": {
            "style": {
                "color": "#555",
                "fontWeight": "bold",
                "fontSize": 12
            }
        },
        "title": {
            "margin": 16,
            "style": {
                "fontWeight": "bold",
                "fontSize": "14px",
                "color": "#65355E"
            }
        },
        "isDirty": True
    },
    "legend": {
        #"align": 'left',
        #"verticalAlign": 'top',
        "backgroundColor": "#FFF",
        "borderColor": "#E0E0E0",
        "borderWidth": 1,
        "padding": 16,
        "margin": 4,
        "itemDistance": 50,
    },
    "title": {
        "align": "left",
        "margin": 0,
        "useHTML": True,
        "style": {
            "fontSize": "18px",
            "padding": "12px 120px 4px 30px",
            #"borderBottom": "1px solid #BBB",
            "borderTop": "1px solid #BBB",
            "borderLeft": "1px solid #BBB",
            "color": "#333",
            #"left": "0px !important"
        },
        #"x": 50
    },
    "subtitle": {
        "align": "left",
        "margin": 0,
        "useHTML": True,
        "style": {
            "fontSize": "13px",
            "padding": "11px 0px 8px 45px",
            "borderLeft": "1px solid #BBB",
            "color": "#555",
            "fontStyle": "italic",
        },
        #"x": 50
    },
    "series": [
    ],
    "plotOptions": {
        "series": {
            "animation": False
        },
        "spline": {
            "lineWidth": 3,
            "marker": {
                "enabled": False
            }
        },
        "line": {
            "lineWidth": 3,
            "marker": {
                "radius": 4,
                "symbol": "circle"
            }
        },
        "areaspline": {
            "lineWidth": 1,
            "marker": {
                "enabled": False
            }
        },
        "area": {
            "lineWidth": 1,
            "marker": {
                "enabled": False
            }
        },
        "boxplot": {
            "fillColor": '#A2B5E2',
            "lineWidth": 1,
            "medianColor": '#333',
            "medianWidth": 4,
            "stemColor": '#555',
            "stemWidth": 3,
            "whiskerColor": '#555',
            "whiskerLength": '30%',
            "whiskerWidth": 4
        }
    },
    "credits": {
        "enabled": False
    }
}


class HighChart(object):

    def __init__(self, render_to, title, ylabel, xlabel, subtitle="-", height=400):
        # default options
        self.chart = deepcopy(DEFAULT_CHART)

        self.set_options("chart", height=height, renderTo=render_to)
        self.set_title( title.upper() )
        self.set_subtitle(subtitle)
        self.set_ylabel(ylabel)
        self.set_xlabel(xlabel)


    def get_chart(self):
        return json.dumps(self.chart)

    def set_options(self, path, **kwargs):
        path = path.split(".")
        self._create_nested_dicts(path)
        obj = self.chart
        for k in path:
            # try to access list items if possible
            try:
                k = int(k)
            except:
                pass
            obj = obj[k]
        obj.update(kwargs)

    def set_title(self, title, **kwargs):
        self.set_options("title", text=title, **kwargs)

    def set_subtitle(self, subtitle, **kwargs):
        self.set_options("subtitle", text=subtitle, **kwargs)

    def set_ylabel(self, label, **kwargs):
        self.set_options("yAxis.title", text=label, **kwargs)

    def set_xlabel(self, label, **kwargs):
        self.set_options("xAxis.title", text=label, **kwargs)

    def set_categories(self, categories, **kwargs):
        tick_interval = (len(categories) / 30) + 1
        self.set_options("xAxis", categories=categories, minTickInterval=tick_interval, **kwargs)

    def plot(self, data, name, **kwargs):
        s = {
            "data": data,
            "name": name
        }
        s.update(kwargs)
        self.chart["series"].append(s)

    def plot_df(self, df, series=None, series_names=None, **kwargs):

        if series is not None:
            # sort series if specified
            if series_names is None:
                raise Exception("You must specify 'series_names' when you specify 'series'")
            if len(series_names) != len(series):
                raise Exception("'series' and 'series_names' must have the same length")

            xy = zip(series, series_names)
            series_names = [y for (x, y) in sorted(xy)]
            series = [x for (x, y) in sorted(xy)]
        else:
            # else sort columns
            cols = sorted(df.columns.tolist())
            df = df[cols]

        series = series or df.columns
        series_names = series_names or df.columns

        self.set_categories(df.index.values.tolist())
        for s, n in zip(series, series_names):
            values = df[s].values.tolist()
            values = [None if np.isnan(value) else value for value in values]
            self.plot(values, n, **kwargs)

    def plot_series(self, series, name=None, **kwargs):
        values = series.values.tolist()
        values = [ None if np.isnan(value) else value for value in values]

        self.set_categories(series.index.values.tolist())
        self.plot(values, name, **kwargs)

    def boxplots(self, df, key, name, percentiles=[.1, .25, .5, .75, .9], type="boxplot", **kwargs):
        percentiles = df.describe(percentiles=percentiles)[key].unstack(level=1)
        dataset = percentiles.to_dict('list')

        mat = [dataset["min"], dataset["25%"], dataset["50%"], dataset["75%"], dataset["max"]]
        mat = np.array(mat).T
        self.plot(mat.tolist(), name, type=type, **kwargs)

        cat = percentiles.index.tolist()
        self.set_categories(cat)


    def hist(self, df, grp, y, bins=15, normed=True, log=False, **kwargs):

        keys = sorted(set(df[grp].values))

        df = df[ [v != None and v != np.NaN  and np.isfinite(v) for v in df[y]] ]
        grouped_df = df.groupby(grp)
        if len(grouped_df) == 0:
            return

        _, bins = np.histogram(df[y].values, bins)
        bin_names = ["%.2f - %.2f" % (bins[i], bins[i+1]) for i in range(len(bins) - 1)]

        all_hists = {k: [0.]*len(bin_names) for k in keys}
        all_hists["bins"] = bin_names

        for grp, df in grouped_df:
            hist, _ = np.histogram(df[y].values, bins, normed=normed)

            if normed and sum(hist) > 0:
                hist /= sum(hist)
                hist *= 100.

            all_hists[grp] = hist

        if log:
            for k, hist in all_hists.items():
                # log(0) is a bitch
                all_hists[k] = [v if v > 0 else 0.0000001 for v in hist]

        df_hists = pd.DataFrame(all_hists)
        df_hists = df_hists.set_index("bins")
        self.plot_df(df_hists, type="column", **kwargs)

        if normed:
            self.set_options("yAxis", max=100)
            self.set_options("tooltip", valueSuffix="%")

        if log:
            all_hists.pop("bins")
            all_values = np.concatenate(all_hists.values())
            min_ = np.min( [v for v in all_values if v != 0.0000001] )
            self.set_options("yAxis", type="logarithmic", min=min_/2)


    def _create_nested_dicts(self, keys):
        """ Adds nested empty dictionary to the chart
        """
        obj = self.chart
        for k in keys:
            # cast k as an integer if possible
            try:
                k = int(k)
            except:
                pass
            if type(obj) == dict and k not in obj:
                obj[k] = {}
            obj = obj[k]


class TwoAxisHighChart(HighChart):
    """ This one has two parallel yAxis, one on the left and one on the right
    """

    def __init__(self, render_to, title, ylabel, xlabel, subtitle="subtitle", height=400):

        super(TwoAxisHighChart, self).__init__(render_to, title=title, subtitle=subtitle,
                ylabel=ylabel, xlabel=xlabel, height=height)

        self.chart["yAxis"] = [
            deepcopy(DEFAULT_CHART["yAxis"]),
            deepcopy(DEFAULT_CHART["yAxis"])
        ]
        self.set_ylabel(ylabel)
        self.set_options("yAxis.1", opposite=True)

    def set_ylabel(self, label, **kwargs):
        self.set_options("yAxis.0.title", text=label[0], **kwargs)
        self.set_options("yAxis.1.title", text=label[1], **kwargs)

def set_rgba_opacity(color, opacity):
    """ Takes an rgba code as a string and modify its opacity
    """
    rgb = color.split(',')[:-1]
    return ','.join(rgb) + ", %f)" % opacity


