

$(document).on("click", ".toggleable", function() {
    var toggleid = $(this).data("toggleid");
    $("#"+toggleid).toggle();
    $(this).find(".glyphicon").toggle();
});


$(document).on("click", ".toggleable-box .toggleable-header", function() {
    var togglebox = $(this).parent();
    $(togglebox).find(".toggleable-content").toggle();
    $(this).find(".icon").toggle();

    $(togglebox).toggleClass("openned")

    $(window).resize();
});


$(document).on("click", ".tabulations .tabulation-header", function() {
    var contents = $(this).parents(".tabulations").find(".tabulation-contents");
    $(this).parent().find(".tabulation-header.active").removeClass("active");
    $(contents).find(".tabulation-content.active").removeClass("active");

    var i = $(this).parent().children().index(this);
    $($(this).parent().children().get(i)).addClass("active");
    $($(contents).children().get(i)).addClass("active");

    $(window).resize();
});



